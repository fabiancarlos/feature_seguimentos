<?php

class Application_Model_Seguimento
{


	public function select(){

		$dao = new Application_Model_DbTable_Seguimentos();
		$select = $dao->select()
				// setIntegrityCheck parea false se não falha o select com join
				->setIntegrityCheck(false)
				->distinct()
				->from(array('s' => 'seguimentos'));

		return $dao->fetchRow($select)->toArray();
		
	}
}

