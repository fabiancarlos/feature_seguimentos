

jQuery(function(){

	// console.log(window.location.pathname);

	// $('#seguimento').change(function(){

	// 	if( $(this).val() ) {

	// 		$('#tiposprodutos').hide();

	// 		$('.carregando').show();

	// 		$.getJSON(window.location.pathname + 'index/tiposprodutos/?search=', { seguimento: $(this).val(), ajax: 'true'} , 

	// 			function(j){

	// 			var options = '<option value=""></option>';	

	// 			for (var i = 0; i < j.length; i++) {
	// 				options += '<option value="' + j[i].tiposprodutos + '">' + j[i].nome + '</option>';
	// 			}
	// 			$('#tiposprodutos').html(options).show();

	// 			$('.carregando').hide();
	// 		});
	// 	} else {
	// 		$('#tiposprodutos').html('<option value="">-- Escolha um estado --</option>');
	// 	}
	// });
	

	/* 	
		Accordion Demo #2
		http://www.geertdedeckere.be/shop/accordionza/
	*/

	// $('#accordion2').accordionza({
	// 	autoPlay: true,
	// 	autoRestartDelay: 3000,
	// 	onSlideClose: function() {
	// 		this.children('p').stop(true).animate({left: 320, opacity: 0}, 500);

	// 		// block tipos produtos
	// 		this.children('.tiposprodutos').stop(true).animate({left: 320, opacity: 0}, 200);
	// 	},
	// 	onSlideOpen: function() {
	// 		var properties = {left: 160, opacity: 1};
	// 		var duration = 500;
	// 		var easing = 'easeOutBack';
	// 		this.children('p').stop(true)
	// 			.filter(':eq(0)').animate({opacity: 0}, 000).animate(properties, duration, easing).end()
	// 			.filter(':eq(1)').animate({opacity: 0}, 250).animate(properties, duration, easing).end()
	// 			.filter(':eq(2)').animate({opacity: 0}, 500).animate(properties, duration, easing);

	// 		// block tipos produtos
	// 		this.children('.tiposprodutos').stop(true).filter(':eq(0)').animate({opacity: 0}, 000).animate(properties, duration, easing).end();
	// 	},
	// 	slideDelay: 3000,
	// 	slideEasing: 'easeOutCirc',
	// 	slideSpeed: 250,
	// 	slideTrigger: 'click',
	// 	slideWidthClosed: 80
	// });


	/*
		PRINCIPAL
		> http://www.geertdedeckere.be/shop/accordionza/
		demo: http://www.geertdedeckere.be/shop/accordionza/download/accordion2/demo.html
		
	*/
	console.log($('#segmento'));

	$('#segmento').accordionza({

		autoPlay: false,
		autoRestartDelay: false,
		onSlideClose: function() {
			this.children('p').stop(true).animate({left: 320, opacity: 0}, 500);

			// block tipos produtos
			this.next('.tiposprodutos').stop(true).animate({opacity: 0}, 200).css('display', 'none');
		},
		onSlideOpen: function() {
			var properties = { left: 0, opacity: 1};
			var duration = 500;
			var easing = 'easeOutBack';

			this.children('p').stop(true)
				.filter(':eq(0)').animate({opacity: 0}, 000).animate(properties, duration, easing).end()
				.filter(':eq(1)').animate({opacity: 0}, 250).animate(properties, duration, easing).end()
				.filter(':eq(2)').animate({opacity: 0}, 500).animate(properties, duration, easing);

			// block tipos produtos
			this.next('.tiposprodutos').stop(true).animate({ opacity: 1}, 000).css('display', 'block').end();

		},
		slideDelay: 3000,
		slideEasing: 'easeOutCirc',
		slideSpeed: 250,
		slideTrigger: 'click',
		slideWidthClosed: 80

	});

});

